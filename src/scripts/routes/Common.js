// JS components
import TypeFont from '../components/TypeFont';
import '../vendors/featherlight';

export default {
  init() { // scripts here run on the DOM load event
    TypeFont.init();
  },
  finalize() { // scripts here fire after init() runs
  //  LazyVideo.init();
  },
};
